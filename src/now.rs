use std::ops::Add;
use std::time::{SystemTime, UNIX_EPOCH, Duration};
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name="basic")]
struct Opt {
    #[structopt(short, default_value="0")]
    added: isize,
    #[structopt(short, default_value="h")]
    unit: String,
}

fn main() {
    let opt = Opt::from_args();
    let now = SystemTime::now();
    println!("Current time: {}", now.duration_since(UNIX_EPOCH).unwrap().as_millis());
    let mut delta: Option<Duration> = None;
    if opt.added != 0 {
        // user requested an interval
        let minute: u64 = 60;
        let hour = minute * 60;
        match opt.unit.as_str() {
            "d" => {
                let gap = hour * 24 * (opt.added as u64);
                let d = Duration::from_secs(gap as u64);
                delta = Some(d);
            }
            "h" => {
                // adjusting hours
                let gap = hour * (opt.added as u64);
                let d = Duration::from_secs(gap as u64);
                delta = Some(d);
            }
            "m" => {
                // adjusting minutes
                let gap = minute * (opt.added as u64);
                let d = Duration::from_secs(gap as u64);
                delta = Some(d);
            }
            _ => {
                println!("Time unit {} cannot be handled, possible values: d, h, and m", opt.added);
            }
        }
        match delta {
            Some(d) => {
                let x = now.add(d);
                println!("Added {}{}: {}", opt.added, opt.unit, x.duration_since(UNIX_EPOCH).unwrap().as_millis());
            }
            _ => {
                // nothing to do
            }
        }
    }
}