use uuid::Uuid;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name="basic")]
struct Opt {
    #[structopt(short, default_value="1")]
    samples: usize,
    #[structopt(short, default_value = "")]
    prefix: String,
}

fn main() {
    let opt = Opt::from_args();
    let sample_count = opt.samples.max(1);
    println!("Generating {} uuids with prefix='{}'\n", sample_count, opt.prefix);
    let prefix = opt.prefix.as_str();
    for _ in 0..sample_count {
        let id = Uuid::new_v4();
        println!("{}{}", prefix, id);
    }
}
